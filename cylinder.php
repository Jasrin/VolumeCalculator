<?php

include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Cylinder;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cylinder1 = new Cylinder();
$cylinder1->pi = 3.14159;
$cylinder1->height = 6;
$cylinder1->radius=4;



$displayer=new Displayer();
$displayer->displaypre($cylinder1->getCylin());
$displayer->displayH1($cylinder1->getCylin());
$displayer->displaysimple($cylinder1->getCylin());

