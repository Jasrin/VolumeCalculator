<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/29/2018
 * Time: 8:40 PM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cone
{
    public $radius;
    public $pi;
    public $height;


    public function getCon()
    {
        return $this->pi * $this->radius * $this->radius * $this->height/3;
    }

}