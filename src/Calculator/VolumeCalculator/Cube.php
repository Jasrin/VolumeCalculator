<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/29/2018
 * Time: 8:40 PM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $side_length;

    public function getSL()
    {
        return $this->side_length * $this->side_length * $this->side_length;
    }

}