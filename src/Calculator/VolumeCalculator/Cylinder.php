<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/29/2018
 * Time: 8:41 PM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{
    public $width ;
    public $length;
    public $height;

    public function getVol()
    {
        return $this->width * $this->length * $this->height;
    }

}