<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/29/2018
 * Time: 10:05 PM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Displayer
{
    function displaysimple($story){
        echo $story;
    }
    function displaypre($story){
        echo "<pre>";
        echo $story;
        echo "</pre>";
        echo "<hr/>";
    }

    function displayH1($story){
        echo "<h1>";
        echo $story;
        echo "</h1>";
    }

}