<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/29/2018
 * Time: 8:39 PM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Volume
{
    public $width ;
    public $length;
    public $height;

    public function getVol()
    {
        return $this->width * $this->length * $this->height;
    }

}