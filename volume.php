<?php

include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Volume;
use Pondit\Calculator\VolumeCalculator\Displayer;

$volume1 = new Volume();
$volume1->width = 10;
$volume1->height = 30;
$volume1->length=10;



$displayer=new Displayer();
$displayer->displaypre($volume1->getVol());
$displayer->displayH1($volume1->getVol());
$displayer->displaysimple($volume1->getVol());
